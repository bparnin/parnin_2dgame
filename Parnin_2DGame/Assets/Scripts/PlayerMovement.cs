﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;


public class PlayerMovement : MonoBehaviour
{

    Rigidbody2D rB2D;
    public float runSpeed;
    public float jumpSpeed;
    public Animator animator;
    public AudioSource m_AudioSource; // refrence audio source
    public TextMeshProUGUI CountText;
    public TextMeshProUGUI HealthText; // Health text
    public SpriteRenderer spriteRenderer;
    public int health;
    int Count = 0;
    bool IsJump;
    // Start is called before the first frame update
    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
        SetCountText();
        SetHealthText(); // set health to text
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            int levelMask = LayerMask.GetMask("Level"); // Gets the layer masks the player can jump from 

            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask)) // check to see if boxcast hits anything
            {
                Jump();

            }
        }
    }




    private void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal"); // adds horzontal input

        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);

        if (rB2D.velocity.x > 0f)
        {
            spriteRenderer.flipX = false; //flip player
        }
        else
        if (rB2D.velocity.x < 0f)
            spriteRenderer.flipX = true;

        if (Mathf.Abs(horizontalInput) > 0f) // if horizontal input is greater than 0f
        {

            animator.SetBool("IsRunning", true); //player is running
            //m_AudioSource.Play();

            //if (!m_AudioSource.isPlaying)
            //{
            //print("hello");
            //m_AudioSource.Play();
            //}
        }
        else
            animator.SetBool("IsRunning", false);
        //m_AudioSource.Stop();

        if (IsJump == true)
        {
            animator.SetBool("IsJumping", true);
        }
        if (IsJump == false)
        {
            animator.SetBool("IsJumping", false);

        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Pickup")) //if player hits pickup
        {
            Count++;
            SetCountText();
            other.gameObject.SetActive(false); // destroy pickup
        }
        if (other.gameObject.CompareTag("Spike")) // hits spikes
        {
            health--;            //health goes down by one
            SetHealthText();
            if (health <=0)
            {
                GameOver();// calls gameover
            }
        }


    }
    void GameOver()
    {
        SceneManager.LoadScene(0); //reload scene
    }
    void Jump()
    {
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpSpeed);


    }
    void SetCountText()
    {
        CountText.text = "Count: " + Count.ToString(); // add   count to canvas
    }
    void SetHealthText()
    {
        HealthText.text = "health: " + health.ToString(); // add   health to canvas
    }
}
